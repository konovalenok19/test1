import React from 'react';
import './badge.css';

const Badge = (props) => {

  let { text, ...other } = props;
  text = text ? text : props.children;

  return (
    <span
      className="badge" {...other}>
      {text}
    </span>
  );
};

export default Badge;