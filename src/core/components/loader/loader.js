import React from 'react';

import { Colors } from '../../utils';

import './loader.css';

const Loader = (props) => {
  const {
    size = '15px',
    color = '#FFFFFF',
    message = null
  } = props;

  return (
    <div className="loader">
      <div
        className="loader-base"
        style={{
          width: size,
          height: size,
          borderColor: Colors.hexToRgba(color, 0.2),
          borderTopColor: color,
        }}
      />
      <div
        className="loader-text"
        style={{
          color: `${Colors.hexToRgba(color, 0.9)}`,
        }}
      >
        {message}
      </div>
    </div>
  );
};

export default Loader;
