import React, { Component, Fragment } from 'react';

import Loader from '../../core/components/loader';
import Badge from '../../core/components/badge';
import {
  fetchPolygon,
  getPolygonMaxValues,
  calcPolygonSquare
} from '../../api.js';

import './super-chart.css';

class SuperChart extends Component {

  static numberToPixels(number) {
    return `${number}px`;
  }

  static polygonToSVGPoints(polygon) {
    return polygon
            .map(point => `${point[0]},-${point[1]}`)
            .reduce((a, b) => `${a} ${b}`);
  }

  constructor() {
    super();

    this.state = {
      isLoaded: false,
      padding: 2,
      polygon: [],
      polygonArea: 0,
      polygonMaxValues: {
        xMax: 0,
        yMax: 0,
      },
      zoom: false,
    };
  }

  handlePressEnter = (e) => {
    const { zoom } = this.state;

    if (e.keyCode === 13) {
      this.setState({
        zoom: !zoom,
      });
    }
  };

  componentDidMount() {
    fetchPolygon().then((response) => {
      const polygonMaxValues = getPolygonMaxValues(response);

      response.unshift([0, 0]);
      response.push([polygonMaxValues.xMax, 0]);

      this.setState({
        isLoaded: true,
        polygon: response,
        polygonMaxValues: polygonMaxValues,
        polygonArea: calcPolygonSquare(response),
      });
    });

    document.addEventListener('keypress', this.handlePressEnter, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keypress', this.handlePressEnter, false);
  }

  render() {
    const { width, height } = this.props;
    const {
      isLoaded,
      padding,
      polygon,
      polygonArea,
      polygonMaxValues: {
        xMax,
        yMax
      },
      zoom,
    } = this.state;

    const chartClass = zoom ? 'super-chart zoomed' : 'super-chart';

    return (
      <div
        className={ chartClass }
        style={{
          width:  this.constructor.numberToPixels(width),
          height: this.constructor.numberToPixels(height),
        }}
      >
        <h1>Super Chart</h1>

        {isLoaded
          ? <svg
              width='100%'
              height='100%'
              viewBox={`-${padding} -${yMax + padding} ${xMax + padding * 2} ${yMax + padding * 2}`}
              preserveAspectRatio="none"
            >
              <polyline
                points={this.constructor.polygonToSVGPoints(polygon)}
                fill="#4E5A7D"
                stroke="#7E91C9"
                strokeWidth="0.5"
              />
            </svg>
          : <Loader
              size={35}
              message={'Загрузка...'}
            />
        }
        <div className="super-chart__analytics">
          {isLoaded &&
            <Fragment>
              <Badge text={`max [${xMax}, ${yMax}]`}/>
              <Badge text={`area ${polygonArea}`}/>
            </Fragment>}
        </div>
      </div>
    );
  }
}

export default SuperChart;
