// Создание точки [x, y]
const createPoint = (v, k) => [k, Math.round((20 * Math.random()) + 60)];


// Создание полигона точек [[x1, y1], [x2, y2], ...]
const createPolygon = () => Array.from({ length: 101 }, createPoint);


// Эмуляция запроса полигона точек
export const fetchPolygon = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve(createPolygon());
  }, 1000);
});


// Возвращает объект с максимальными значениями
export const getPolygonMaxValues = (polygon) => {

  // Поиск max X (берем последний)
  const xMax = polygon[polygon.length - 1][0];

  // Поиск max Y
  const yMax = polygon.reduce((prev, curr) => {
    return prev[1] > curr[1] ? prev : curr;
  })[1];

  return { xMax, yMax };
};


// Подсчет площади под графиком полигона
export const calcPolygonSquare = (polygon) => {

  // текущая точка и предыдущая, площадь
  let current, prev, area = 0;
  const { length } = polygon;

  for (let i = 1; i < length; ++i) {
    current = polygon[i];
    prev = polygon[i - 1];

    // Площадь прямоугольника (шаг равен 1)
    let rectangleArea = current[1];

    // Площадь треугольника
    let triangleArea = (current[1] - prev[1]) / 2;

    // параметр - прибавляем или вычитаем площадь треугольника
    let param = 1;

    // Если текующая точка > предыдущей по Y, то вычитаем площадь треугольника
    if (current[1] < prev[1]) param = -1;

    area += rectangleArea + param * triangleArea;
  }

  return area;
};
